parameters {
        activeChoiceParam('BUILD_CLUSTER_NAME') {
            description('Which MTC2 cluster will this be provisioned and installed on?')
            choiceType('SINGLE_SELECT')
            groovyScript {
                script('["aws_env_b3", "aws_env_b", "aws_env_c3", "aws_env_e"]')
            }
        activeChoiceParam('DESTRUCTIVE_LOAD') {
            description('Destroy the currently deployed cluster and reload the OSs?')
            choiceType('SINGLE_SELECT')
            groovyScript {
                script('["true","false"]')
            }
        activeChoiceReactiveParam('AWS_VPC') {
            description('Which AWS VPC should this be installed in?\nmtc2-prod    The standard VPC that most of MTC2 clusters are in\nmtc2-devtest    The other VPC that has additional IP space')
            choiceType('SINGLE_SELECT')
            groovyScript {
                script('if (DESTRUCTIVE_LOAD.equals("true")) { return["mtc2-prod", "mtc2-devtest"] } else if (DESTRUCTIVE_LOAD.equals("false")) { return }')
            }
            referencedParameter('DESTRUCTIVE_LOAD')
        }
        activeChoiceReactiveParam('NIGHTLY_SHUTDOWN') {
            description('Which AWS nightly shutdown option should be used for the cluster? Defaults to standard.\nstandard    The cluster is powered off at 19:00\nignore    The cluster is not powered off at night. Please only use as necessary!')
            choiceType('SINGLE_SELECT')
            groovyScript {
                script('if (DESTRUCTIVE_LOAD.equals("true")) { return["standard", "ignore"] } else if (DESTRUCTIVE_LOAD.equals("false")) { return }')
            }
            referencedParameter('DESTRUCTIVE_LOAD')
        }
        activeChoiceReactiveReferenceParam('AMI') {
            description('Enter the AMI to use during terraform. Defaults to ami-e98eca88')
            choiceType('FORMATTED_HTML')
            groovyScript {
                script('if (DESTRUCTIVE_LOAD.equals("true")) { return "<input type=\"text\" name=\"value\" value=\"ami-e98eca88\" />" } else { return }')
            }
            referencedParameter('DESTRUCTIVE_LOAD')
        }
        activeChoiceParam('PREP_ANSIBLE') {
            description('Pull the selected branch for Ansible playbooks from MTC2-infrastructure, prep the inventory file and Ansible working directory')
            choiceType('SINGLE_SELECT')
            groovyScript {
                script('["true","false"]')
            }
        }
        activeChoiceReactiveReferenceParam('ANSIBLE_BRANCH') {
            description('Enter the branch name, tag, or commit hash to be checked out for the mtc2-infrastructure Ansible playbooks. Defaults to the develop branch')
            choiceType('FORMATTED_HTML')
            groovyScript {
                script('if (PREP_ANSIBLE.equals("true")) { return "<input type=\"text\" name=\"value\" value=\"develop\" />" } else { return }')
            }
            referencedParameter('PREP_ANSIBLE')
        }
        activeChoiceParam('STAGE_ARTIFACTS') {
            description('Download latest artifacts and stage them under PIPELINE')
            choiceType('SINGLE_SELECT')
            groovyScript {
                script('["true","false"]')
            }
        }
        activeChoiceReactiveReferenceParam('PACKAGE_BRANCH') {
            description('Enter the branch name to use for staged artifacts')
            choiceType('FORMATTED_HTML')
            groovyScript {
                script('if (STAGE_ARTIFACTS.equals("true")) { return "<input type=\"text\" name=\"value\" value=\"develop\" />" } else { return }')
            }
            referencedParameter('STAGE_ARTIFACTS')
        }
        activeChoiceParam('RUN_ANSIBLE') {
            description('Run Ansible MTC2_install.yml playbook')
            choiceType('SINGLE_SELECT')
            groovyScript {
                script('["true","false"]')
            }
        }
        activeChoiceReactiveParam('GCCSM') {
            description('Install GCCS-M Components')
            choiceType('SINGLE_SELECT')
            groovyScript {
                script('if (RUN_ANSIBLE.equals("true")) { return["true", "false"] } else if (RUN_ANSIBLE.equals("false")) { return }')
            }
            referencedParameter('RUN_ANSIBLE')
        }
        activeChoiceReactiveParam('TPT') {
            description('Install TPT Components')
            choiceType('SINGLE_SELECT')
            groovyScript {
                script('if (RUN_ANSIBLE.equals("true")) { return["true", "false"] } else if (RUN_ANSIBLE.equals("false")) { return }')
            }
            referencedParameter('RUN_ANSIBLE')
        }
        activeChoiceReactiveReferenceParam('PACKAGE_VERSION') {
            description('Enter the package version to use for the Ansible installation. This will be appended on to the --extra-vars parameters for all packages, e.g. opt_gui_version=2018-RC1 if you entered 2018-RC1')
            choiceType('FORMATTED_HTML')
            groovyScript {
                script('if (RUN_ANSIBLE.equals("true")) { return "<input type=\"text\" name=\"value\" value=\"\" />" } else { return }')
            }
            referencedParameter('RUN_ANSIBLE')
        }
        activeChoiceParam('CONFIGURE_ACAS') {
            description('Run scripts necessary for performing RDTE ACAS scans')
            choiceType('SINGLE_SELECT')
            groovyScript {
                script('["true","false"]')
            }
        }

    }        